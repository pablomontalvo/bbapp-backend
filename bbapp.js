/**
  BRIDGE BOOKING APP
**/
var app = require('express')();
var moment = require('moment');
var bodyParser = require('body-parser');
var Datastore = require('nedb');

var bcdb = new Datastore({
  filename: 'bridgecatalog.db',
  autoload: true,
  timestampData: true
});

var bbdb = new Datastore({
  filename: 'bridgebooking.db',
  autoload: true,
  timestampData: true
});

bbdb.ensureIndex({fieldName : 'startDatetime'});
bbdb.ensureIndex({fieldName : 'endDatetime'});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true
}));

/**
  EXPRESS
**/

var listener = app.listen(8001, function(){
  console.log('Bridge Booking App listening on port ' + listener.address().port + '...');
});

/**
    BRIDGE CATALOG
**/

// GET entire bridges catalog
app.get('/bbapp/bridge/catalog', function(req, res) {
  console.log("->Getting the list of available bridges");
  bcdb.find({}).sort({ createdAt: 1 }).exec(function (err, catalog) {
    if (err) res.send(err);
    var payload = {
      "bridgeCatalogList": catalog
    };
    res.json(payload);
  });
});

// INSERT new bridge into catalog
app.post('/bbapp/bridge/insert', function(req, res) {
  console.log("->Inserting new bridge into catalog");
  var bridge = req.body;
  bcdb.insert(bridge, function(err, bridge) {
    if (err) res.send(err);
    console.log("   New bridge created with id: " + bridge._id);
    res.json(bridge);
  });
});

// DELETE a bridge from catalog
app.post('/bbapp/bridge/delete/:id', function(req, res) {
  var id = req.params.id;
  console.log("->Removing bridge with id "+ id +" from catalog");
  bcdb.remove({
    _id: id
  }, {}, function(err, id) {
    if (err) res.send(err);
    res.sendStatus(200);
  });
});
/********************************************************/

/**
    BRIDGE BOOKINGS
**/

// INSERT new bridgebooking
app.post('/bbapp/bridge/booking/create', function(req, res) {
  var booking = req.body;
  console.log("->Creating new booking from: " + booking.startDatetime + ' to: ' + booking.endDatetime);
  booking.startDatetime = moment(new Date(booking.startDatetime)).toDate();
  booking.endDatetime = moment(new Date(booking.endDatetime)).toDate();
  bbdb.insert(booking, function(err, booking) {
    if (err) res.send(err);
    console.log("   New booking registered with id: " + booking._id);
    res.json(booking);
  });
});

// DELETE existent booking
app.post('/bbapp/bridge/booking/release', function(req, res) {
  var booking = req.body;
  console.log("->Removing booking with id "+ booking._id);
  bbdb.remove({
    _id: booking._id
  }, {}, function(err, booking) {
    if (err) res.send(err);
    //res.sendStatus(200);
    res.json(booking);
  });
});

// GET all bookings by date range
app.get('/bbapp/bridge/booking/get', function(req, res) {
  var from = moment(new Date(req.query.startDateTime)).toDate();
  var to = moment(new Date(req.query.endDateTime)).toDate();
  console.log("->Client looking for an slot from: "+ from + " to: " + to + " on bridge: " + req.query.bridgeId);
  bbdb.find({
    bridgeId: req.query.bridgeId,
    $or: [{
    $and: [{
        startDatetime: {
            $gte: from
        }
    }, {
        startDatetime: {
            $lte: to
        }
    }]
}, {
    $and: [{
        endDatetime: {
            $gte: from
        }
    }, {
        endDatetime: {
            $lte: to
        }
    }]
}]
  }).sort({ createdAt: 1 }).exec(function(err, results) {
    if (err) res.send(err);
    console.log("   Number of items retrieved: " + Object.keys(results).length);
    var payload = {
      "bridgeBookingList": results
    };
    res.json(payload);
  });
});

// GET all bridges booked at specific day
app.get('/bbapp/bridge/booking/getbydate', function(req, res) {
  var from = moment(new Date(req.query.startDate)).hour(00).minute(00).second(00)
  var to = moment(new Date(req.query.endDate)).hour(23).minute(59).second(59)
  from = from.toDate()
  to = to.toDate()
  console.log("->Querying the list of bookings from: "+ from + " to: " + to + " on bridge: " + req.query.bridgeId);
  bbdb.find({
    bridgeId: req.query.bridgeId,
    startDatetime: { $gte: from },
    endDatetime: { $lte: to }
  }).sort({ createdAt: 1 }).exec(function(err, results) {
    if (err) res.send(err);
    console.log("   Number of items retrieved: " + Object.keys(results).length);
    var payload = {
      "bridgeBookingList": results
    };
    res.json(payload);
  });
});
