// GET a bridge by its id
app.get('/bridgecatalog/get/:id', function(req, res) {
  var id = +req.params.id;
  bcdb.find({
    bridgeId: id
  }, {}, function(err, bridge) {
    if (err) res.send(err);
    res.json(bridge);
  });
});

// GET a bridge by its internal _id
app.get('/bridgecatalog/getbyid/:id', function(req, res) {
  var id = req.params.id;
  bcdb.findOne({
    _id: id
  }, {}, function(err, bridge) {
    if (err) res.send(err);
    res.json(bridge);
  });
});

// UPDATE (upsert) an existent or new bridge into catalog
app.post('/bridgecatalog/update', function(req, res) {
  var bridge = req.body;
  bcdb.update({ bridgeId: bridge.bridgeId }, bridge, { upsert: true }, function(err, bridge) {
    if (err) res.send(err);
    res.json(bridge);
  });
});

// UPDATE (upsert) an existent bridge into catalog
app.post('/bridgecatalog/update/owner', function(req, res) {
  var bridge = req.body;
  bcdb.update({ bridgeId: bridge.bridgeId }, { $set: {owner: bridge.owner}
  }, { upsert: true }, function(err, bridge) {
    if (err) res.send(err);
    res.json(bridge);
  });
});
